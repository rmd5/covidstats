import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class Dictionary extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: null,
			global: [{
				"NewConfirmed": 0,
				"NewDeaths": 0,
				"NewRecovered": 0,
				"TotalConfirmed": 0,
				"TotalDeaths": 0,
				"TotalRecovered": 0
			}],
			countries: [{
				"Country": "",
				"CountryCode": "",
				"Date": "",
				"NewConfirmed": 0,
				"NewDeaths": 0,
				"NewRecovered": 0,
				"Slug": "",
				"TotalConfirmed": 0,
				"TotalDeaths": 0,
				"TotalRecovered": 0
			}],
			countryNames: [],
			countryOptions: [],
			pages: [],
			search: null
		}
	}

	async componentDidMount() {
		const url = "https://api.covid19api.com/summary";

		const response = await fetch(url);
		const data = await response.json();
		this.setState({
			data: data
		}, () => {
			this.sortData();
		});
	}

	sortData() {
		let data = this.state.data;

		let global = data.Global;
		let countries = data.Countries;

		this.setState({
			global: global,
			countries: countries
		}, () => {
			this.sortCountries();
		});
	}

	sortCountries() {
		let countries = this.state.countries;
		let pages = [];
		let names = [];
		let options = [];

		for (let i = 0, size = countries.length; i < size; i++) {
			let name = countries[i].Country;
			if (name === "Venezuela (Bolivarian Republic)") {
				name = "Venezuela";
			}
			let option = <option value={name}></option>;

			let page =
				<div className="card" id={name}>
					<div className="cardHeading">
						{name}
					</div>
					<table>
						<tbody>
							<tr>
								<td>
									<span className="sectionHeading">
										New Confirmed:
									</span>
								</td>
								<td>
									{countries[i].NewConfirmed}
								</td>
							</tr>
							<tr>
								<td>
									<span className="sectionHeading">
										New Deaths:
									</span>
								</td>
								<td>
									{countries[i].NewDeaths}
								</td>
							</tr>
							<tr>
								<td>
									<span className="sectionHeading">
										New Recovered:
									</span>
								</td>
								<td>
									{countries[i].NewRecovered}
								</td>
							</tr>
							<tr>
								<td>
									<span className="sectionHeading">
										Total Confirmed:
									</span>
								</td>
								<td>
									{countries[i].TotalConfirmed}
								</td>
							</tr>
							<tr>
								<td>
									<span className="sectionHeading">
										Total Deaths:
									</span>
								</td>
								<td>
									{countries[i].TotalDeaths}
								</td>
							</tr>
							<tr>
								<td>
									<span className="sectionHeading">
										Total Recovered:
									</span>
								</td>
								<td>
									{countries[i].TotalRecovered}
								</td>
							</tr>
						</tbody>
					</table>
				</div>;

			options.push(option);
			names.push(name);
			pages.push(page);
		}

		this.setState({
			pages: pages,
			countryNames: names,
			countryOptions: options
		}, () => {
		});
	}

	updateSearch = (e) => {
		if (this.state.countryNames.indexOf(e.target.value) > -1) {
			this.setState({
				search: e.target.value
			}, () => {
				document.getElementById(this.state.search).style.transform = "scale(1.2)";
				document.getElementById(this.state.search).scrollIntoView({ behavior: "smooth", block: "center" });
			});
		} else {
			if (this.state.search !== null && this.state.search !== undefined) {
				document.getElementById(this.state.search).style.transform = "scale(1)";
			}

			this.setState({
				search: null
			});
		}
	}

	searchCountry = () => {
		if (this.state.search !== null && this.state.search !== undefined && document.getElementById(this.state.search)) {
			document.getElementById(this.state.search).scrollIntoView({ behavior: "smooth", block: "center" });
		}
	}

	render() {
		return (
			<div className="analysis">
				<div className="card globalCard">
					<div className="cardHeading">
						Global Covid19 Stats
					</div>
					<table>
						<tbody>
							<tr>
								<td>
									<span className="sectionHeading">
										New Confirmed:
									</span>
								</td>
								<td>
									{this.state.global.NewConfirmed}
								</td>
							</tr>
							<tr>
								<td>
									<span className="sectionHeading">
										New Deaths:
									</span>
								</td>
								<td>
									{this.state.global.NewDeaths}
								</td>
							</tr>
							<tr>
								<td>
									<span className="sectionHeading">
										New Recovered:
									</span>
								</td>
								<td>
									{this.state.global.NewRecovered}
								</td>
							</tr>
							<tr>
								<td>
									<span className="sectionHeading">
										Total Confirmed:
									</span>
								</td>
								<td>
									{this.state.global.TotalConfirmed}
								</td>
							</tr>
							<tr>
								<td>
									<span className="sectionHeading">
										Total Deaths:
									</span>
								</td>
								<td>
									{this.state.global.TotalDeaths}
								</td>
							</tr>
							<tr>
								<td>
									<span className="sectionHeading">
										Total Recovered:
									</span>
								</td>
								<td>
									{this.state.global.TotalRecovered}
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<br />

				<div className="searchBar">
					<label className="search">
						Search Countries: 
					</label>
					<input
						type="text"
						className="search"
						list="countries"
						onChange={this.updateSearch}
					></input>
					<datalist id="countries">
						{this.state.countryOptions}
					</datalist>
				</div>

				<br />

				{this.state.pages}
			</div>
		);
	}
}

ReactDOM.render(
	<React.StrictMode>
		<Dictionary />
	</React.StrictMode>,
	document.getElementById('root')
);